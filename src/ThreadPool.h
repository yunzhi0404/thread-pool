#ifndef __THREDPOOL_H__
#define __THREDPOOL_H__

#include "TaskQueue.h"
#include "TaskQueue.cc"

template <typename T>
class ThreadPool{
public:
    ThreadPool(int min, int max);
    ~ThreadPool();

    void addTask(Task<T> task);
    int getbusyNum();
    int getliveNum();
    
private:
    static void* worker(void* arg);
    static void* manager(void* arg);
    void threadExit();

private:
    TaskQueue<T>* taskQ;
    pthread_t managerID;
    pthread_t* threadIDs;
    int minNum;
    int maxNum;
    int busyNum;
    int liveNum;
    int exitNum;
    pthread_mutex_t mutexPool;
    pthread_cond_t notEmpyt;
    bool shutdown;      // 是否要销毁线程池
    static const int NUMBER = 2;
};

#endif