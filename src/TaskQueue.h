#ifndef __TASKQUEUE_H__
#define __TASKQUEUE_H__

#include <queue>
#include <pthread.h>

using callback = void(*)(void* arg);
template <typename T>
struct Task{
    Task<T>(){
        function = nullptr;
        arg = nullptr;
    }
    Task<T>(callback f, void* arg){
        this->arg = (T*)arg;
        function = f;
    }
    callback function;
    T* arg;
};

template <typename T>
class TaskQueue{
public:
    TaskQueue();
    ~TaskQueue();
    
    // 添加任务
    void addTask(Task<T> task);
    void addTask(callback f, void* arg);
    // 取出任务
    Task<T> takeTask();
    // 获取任务个数
    inline size_t taskNumber() { return m_taskQ.size(); }

private:
    pthread_mutex_t m_mutex;
    std::queue<Task<T>> m_taskQ;
};



#endif // !__TASKQUEUE_H__
